package com.example.auto_valuation.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.auto_valuation.R;
import com.example.auto_valuation.models.AutoEvalItemV2;

import java.util.ArrayList;
import java.util.List;

public class AutoEvalAdapterV2 extends BaseAdapter {

    private Context context;
    private List<AutoEvalItemV2> listeComp;
    private LayoutInflater inflater;
    private static ArrayList<Integer> niveauChoisi;

    public AutoEvalAdapterV2(Context context, List<AutoEvalItemV2> listeComp){
        this.context = context;
        this.listeComp = listeComp;
        this.inflater = LayoutInflater.from(context);
        niveauChoisi = new ArrayList<>();
        for (int i = 0; i < listeComp.size(); i++) {
            niveauChoisi.add(0);
        }
    }

    @Override
    public int getCount() {
        return listeComp.size();
    }

    @Override
    public AutoEvalItemV2 getItem(int position) {
        return listeComp.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        view = inflater.inflate(R.layout.test, null);

        AutoEvalItemV2 currentItem = getItem(position);
        String enonceComp = currentItem.getEnonceComp();

        TextView enonceCompTV = view.findViewById(R.id.enoncecompetence);
        TextView compTV = view.findViewById(R.id.competence);
        RadioButton niv1RB = view.findViewById(R.id.radioButton);
        RadioButton niv2RB = view.findViewById(R.id.radioButton2);
        RadioButton niv3RB = view.findViewById(R.id.radioButton3);
        RadioButton niv4RB = view.findViewById(R.id.radioButton4);


        niv1RB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                niveauChoisi.set(position, 1);
                compTV.setText("Moins de un point par figure (chutes, figures non maîtrisées)");
                compTV.setBackgroundColor(Color.RED);
            }
        });




        niv2RB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                niveauChoisi.set(position, 2);
                compTV.setText("Moyenne d’au moins 1 point par figure (A).");
                compTV.setBackgroundColor(Color.rgb(246, 143, 4));
            }
        });


        niv3RB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                niveauChoisi.set(position, 3);
                compTV.setText("Moyenne de 2 points ou plus par figure(B).");
                compTV.setBackgroundColor(Color.YELLOW);
            }
        });


        niv4RB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                niveauChoisi.set(position, 4);
                compTV.setText("Moyenne de 3 points par figure (C).");
                compTV.setBackgroundColor(Color.GREEN);
            }
        });

        if (niveauChoisi.get(position).equals(1)) {
            niv1RB.setChecked(true);
            compTV.setText("Moins de un point par figure (chutes, figures non maîtrisées)");
            compTV.setBackgroundColor(Color.RED);
        } else if (niveauChoisi.get(position).equals(2)){
            niv2RB.setChecked(true);
            compTV.setText("Moyenne d’au moins 1 point par figure (A).");
            compTV.setBackgroundColor(Color.rgb(246, 143, 4));
        }else if (niveauChoisi.get(position).equals(3)){
            niv3RB.setChecked(true);
            compTV.setText("Moyenne de 2 points ou plus par figure(B).");
            compTV.setBackgroundColor(Color.YELLOW);
        }else if (niveauChoisi.get(position).equals(4)){
            niv4RB.setChecked(true);
            compTV.setText("Moyenne de 3 points par figure (C).");
            compTV.setBackgroundColor(Color.GREEN);
        }


        enonceCompTV.setText(enonceComp);

        return view;
    }
}