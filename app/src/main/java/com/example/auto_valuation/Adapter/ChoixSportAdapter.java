package com.example.auto_valuation.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.auto_valuation.R;
import com.example.auto_valuation.models.SportItem;

import java.util.List;

public class ChoixSportAdapter extends BaseAdapter {

    private Context context;
    private List<SportItem> listeSport;
    private LayoutInflater inflater;

    public ChoixSportAdapter(Context context, List<SportItem> listeSport){
        this.context = context;
        this.listeSport = listeSport;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return listeSport.size();
    }

    @Override
    public SportItem getItem(int position) {
        return listeSport.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup parent) {

        view = inflater.inflate(R.layout.adapter_item_choix_sport, null);

        //On récupère les infos de l'item
        SportItem currentItem = getItem(i);
        String sportName = currentItem.getsportName();

        //On récupère le nom de la vue
        TextView itemNameView = view.findViewById(R.id.sportName);
        itemNameView.setText(sportName);

        return view;
    }
}
