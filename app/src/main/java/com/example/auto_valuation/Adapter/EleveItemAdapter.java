package com.example.auto_valuation.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.auto_valuation.R;
import com.example.auto_valuation.models.EleveItem;
import com.example.auto_valuation.models.SportItem;

import java.util.List;

public class EleveItemAdapter extends BaseAdapter {

    private Context context;
    private List<EleveItem> listeEleve;
    private LayoutInflater inflater;

    public EleveItemAdapter(Context context, List<EleveItem> listeEleve){
        this.context = context;
        this.listeEleve = listeEleve;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return listeEleve.size();
    }

    @Override
    public EleveItem getItem(int position) {
        return listeEleve.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup parent) {
        view = inflater.inflate(R.layout.adapter_item_eleve, null);

        //On récupère les infos de l'item
        EleveItem currentItem = getItem(i);
        String eleveName = currentItem.getName();

        //On récupère le nom de la vue
        TextView itemNameView = view.findViewById(R.id.eleveName);
        itemNameView.setText(eleveName);

        return view;
    }
}
