package com.example.auto_valuation;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;

import com.example.auto_valuation.Adapter.EleveItemAdapter;
import com.example.auto_valuation.models.EleveItem;

import java.util.ArrayList;
import java.util.List;

public class AuthentificationEleve extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private Button valider;
    private ImageButton retourAccueil;
    private  ListView eleveListView;
    private String nomEleve;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.authentification_eleve);

        this.valider = findViewById(R.id.validerEleve);
        this.retourAccueil = findViewById(R.id.retourAuthEleve);

        //Liste élèves
        List<EleveItem> listeEleve = new ArrayList<>();
        listeEleve.add(new EleveItem("Durand Jean"));
        listeEleve.add(new EleveItem("Beauvais Mathis"));
        listeEleve.add(new EleveItem("Castanet Louis"));
        listeEleve.add(new EleveItem("Pelletier Rémi"));
        listeEleve.add(new EleveItem("Beauvais Jean-Jaques"));

        eleveListView = findViewById(R.id.listEleveAuth);

        eleveListView.setAdapter(new EleveItemAdapter(this, listeEleve));

        //selction d'un item de la liste + récuperation du texte
        eleveListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            @SuppressWarnings("unchecked")
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                for(int i=0; i<eleveListView.getChildCount(); i++) {
                   eleveListView.getChildAt(i).setBackgroundColor(getColor(R.color.transparent));
                }
                view.setBackgroundColor(getColor(R.color.checkLV));
                nomEleve = listeEleve.get(position).getName();
            }
        });

        //spinner groupes
        Spinner groupeAuthSpinner = findViewById(R.id.grp);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.groupe, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        groupeAuthSpinner.setAdapter(adapter);
        groupeAuthSpinner.setOnItemSelectedListener(this);

        //Boutons
        valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sceance = new Intent(AuthentificationEleve.this, MenuEleve.class);
                sceance.putExtra("nomEleve", nomEleve);
                startActivity(sceance);
            }
        });

        retourAccueil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent retour = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(retour);
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}