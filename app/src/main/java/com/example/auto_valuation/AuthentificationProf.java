package com.example.auto_valuation;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import androidx.appcompat.app.AppCompatActivity;

public class AuthentificationProf extends AppCompatActivity {

    private Button toMenu;
    private ImageButton retourAccueil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.authentification_prof);

        this.toMenu = findViewById(R.id.valider);
        this.retourAccueil = findViewById(R.id.retourAccueil);

        toMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent menu = new Intent(getApplicationContext(), MenuProf.class);
                startActivity(menu);
            }
        });

        retourAccueil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent retour = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(retour);
            }
        });

    }


}
