package com.example.auto_valuation;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.auto_valuation.Adapter.AutoEvalAdapterV2;
import com.example.auto_valuation.models.AutoEvalItemV2;

import java.util.ArrayList;
import java.util.List;

public class AutoEvaluationV2 extends AppCompatActivity {

    private String nomSport;
    private String numSceance;
    private RadioGroup nivCompRG;
    private Button retour;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.auto_evaluation);

        Intent autoEval = getIntent();

        if (autoEval != null){
            nomSport = "";
            numSceance = "";
            if (autoEval.hasExtra("nomSport")){ // vérifie qu'une valeur est associée à la clé
                nomSport = autoEval.getStringExtra("nomSport");
                numSceance = autoEval.getStringExtra("numSceance");
            }
            TextView titre = findViewById(R.id.titreAutoEval);
            titre.setText("Auto-Evaluation de "  + nomSport + " scéance n° " + numSceance);

        }

        //Affichage de la liste des sports
        List<AutoEvalItemV2> listeComp = new ArrayList<>();
        listeComp.add(new AutoEvalItemV2(
                "Réaliser des figures les plus complexes possible grâce à des voltigeurs renversés"));
        listeComp.add(new AutoEvalItemV2(
                "Rendre les montages et démontages plus acrobatiques"));
        listeComp.add(new AutoEvalItemV2(
                "Adopter des méthodes pour optimiser les enchaînements : répéter / observer / écouter / modifier"));
        listeComp.add(new AutoEvalItemV2(
                "Aider un groupe partenaire à réaliser des choix en appréciant l’effet créé"));


        ListView compListView = findViewById(R.id.listeViewComp);
        compListView.setAdapter(new AutoEvalAdapterV2(this, listeComp));


        //boutons
        retour = findViewById(R.id.retourChoixSport);

        retour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent retourSport = new Intent(getApplicationContext(), Cours.class);
                startActivity(retourSport);
            }
        });

    }
}
