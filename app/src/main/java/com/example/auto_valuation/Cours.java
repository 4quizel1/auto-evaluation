package com.example.auto_valuation;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.auto_valuation.Adapter.ChoixSportAdapter;
import com.example.auto_valuation.models.SportItem;

import java.util.ArrayList;
import java.util.List;

public class Cours extends AppCompatActivity implements AdapterView.OnItemSelectedListener{

    private ImageButton retour;
    private Spinner spinnerNumSceance;
    private String nomEleve;
    private String nomSport;
    private String numSceance;
    private List<SportItem> listeSport;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.choix_sport);

        //Affichage du nom de l'élève
        Intent sceance = getIntent();

        if (sceance != null){
            nomEleve = "";
            if (sceance.hasExtra("nomEleve")){ // vérifie qu'une valeur est associée à la clé
                nomEleve = sceance.getStringExtra("nomEleve"); // on récupère la valeur associée à la clé
            }
            TextView eleve = findViewById(R.id.nomEleve);
            eleve.setText(nomEleve);
        }

        //Affichage de la liste des sports
        listeSport = new ArrayList<>();
        listeSport.add(new SportItem("Basket"));
        listeSport.add(new SportItem("Badminton"));
        listeSport.add(new SportItem("Course d'orientation"));

        ListView sportListView = findViewById(R.id.listeChoixSport);
        sportListView.setAdapter(new ChoixSportAdapter(this, listeSport));

        //Spinner numéro de scéance
        spinnerNumSceance = findViewById(R.id.numCours);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.sceance, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerNumSceance.setAdapter(adapter);
        spinnerNumSceance.setOnItemSelectedListener(this);

        //bouton
        this.retour =findViewById(R.id.retourChoixSport);

        retour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent retourMenu = new Intent(getApplicationContext(),MenuEleve.class);
                startActivity(retourMenu);
            }
        });

        sportListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                nomSport = listeSport.get(position).getsportName();
                numSceance = spinnerNumSceance.getSelectedItem().toString();
                Intent sceance = new Intent(Cours.this, AutoEvaluationV2.class);
                sceance.putExtra("nomEleve", nomEleve);
                sceance.putExtra("nomSport", nomSport);
                sceance.putExtra("numSceance", numSceance);
               startActivity(sceance);
               // Toast.makeText(getApplicationContext(), nomEleve + " " + nomSport + " " + numSceance, Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
