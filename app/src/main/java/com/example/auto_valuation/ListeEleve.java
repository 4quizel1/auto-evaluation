package com.example.auto_valuation;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

import com.example.auto_valuation.Adapter.EleveItemAdapter;
import com.example.auto_valuation.models.EleveItem;

import java.util.ArrayList;
import java.util.List;

public class ListeEleve extends AppCompatActivity implements AdapterView.OnItemSelectedListener{

    private ImageButton retour;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.liste_eleve);

        retour = findViewById(R.id.retourEleve);

        //Liste élèves
        List<EleveItem> listeEleve = new ArrayList<>();
        listeEleve.add(new EleveItem("Durand Jean"));
        listeEleve.add(new EleveItem("Beauvais Mathis"));
        listeEleve.add(new EleveItem("Castanet Louis"));
        listeEleve.add(new EleveItem("Pelletier Rémi"));
        ListView eleveListView = findViewById(R.id.eleveListView);


        eleveListView.setAdapter(new EleveItemAdapter(this, listeEleve));

        //Spinner groupes
        Spinner groupeAuthSpinner = findViewById(R.id.grpListe);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.groupe, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        groupeAuthSpinner.setAdapter(adapter);
        groupeAuthSpinner.setOnItemSelectedListener(this);

        //Boutons
        this.retour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent menu = new Intent(getApplicationContext(), MenuProf.class);
                startActivity(menu);
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
