package com.example.auto_valuation;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.auto_valuation.Adapter.ChoixSportAdapter;
import com.example.auto_valuation.models.CustomPopUp;
import com.example.auto_valuation.models.SportItem;

import java.util.ArrayList;
import java.util.List;

public class ListeSport extends AppCompatActivity {

    private Button newSport;
    private Button afficher;
    private Button supprimer;
    private ImageButton retour;
    private String nomSport;
    private String strNewSport;
    private ListeSport activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.liste_sport);
        this.activity = this;

        List<SportItem> listeSport = new ArrayList<>();
        listeSport.add(new SportItem("Basket"));
        listeSport.add(new SportItem("Badminton"));
        listeSport.add(new SportItem("Course d'orientation"));

       ListView sportListView = findViewById(R.id.listeSport);
       sportListView.setAdapter(new ChoixSportAdapter(this, listeSport));

       this.afficher = findViewById(R.id.afficher);
       this.supprimer = findViewById(R.id.supprimer);

       afficher.setClickable(false);
       supprimer.setClickable(false);

        sportListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            @SuppressWarnings("unchecked")
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                for(int i=0; i<sportListView.getChildCount(); i++) {
                    sportListView.getChildAt(i).setBackgroundColor(getColor(R.color.transparent));
                }
                view.setBackgroundColor(getColor(R.color.checkLV));
                afficher.setClickable(true);
                supprimer.setClickable(true);
                nomSport = listeSport.get(position).getsportName();
            }
        });

        //boutons
        this.newSport = findViewById(R.id.newSport);
        this.retour =findViewById(R.id.retourSport);

       retour.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Intent retourMenu = new Intent(getApplicationContext(), MenuProf.class);
               startActivity(retourMenu);
           }
       });

       newSport.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {

               final CustomPopUp customPopUp = new CustomPopUp(activity);
               customPopUp.getValiderpopUp().setOnClickListener(new View.OnClickListener() {
                   @Override
                   public void onClick(View v) {
                       strNewSport = customPopUp.getSpinner().getSelectedItem().toString();
                       customPopUp.dismiss();
                       listeSport.add(new SportItem(strNewSport));
                       Toast.makeText(getApplicationContext(), strNewSport, Toast.LENGTH_SHORT).show();
                   }
               });

               customPopUp.getRetourPopUp().setOnClickListener(new View.OnClickListener() {
                   @Override
                   public void onClick(View v) {
                       customPopUp.dismiss();
                   }
               });
               customPopUp.build();
           }
       });

    }

}
