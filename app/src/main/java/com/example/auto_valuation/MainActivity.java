package com.example.auto_valuation;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button prof;
    private Button eleve;
    private Button test;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Display display = ((WindowManager) getSystemService(WINDOW_SERVICE)).getDefaultDisplay();

        this.prof = findViewById(R.id.buttonProf);
        this.eleve = findViewById(R.id.buttonEleve);

        prof.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent authProf = new Intent(getApplicationContext(), AuthentificationProf.class);
                startActivity(authProf);
            }
        });

        eleve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent autheleve = new Intent(getApplicationContext(), AuthentificationEleve.class);
                startActivity(autheleve);

            }
        });

    }


}