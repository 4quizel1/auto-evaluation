package com.example.auto_valuation;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MenuEleve extends AppCompatActivity {

    private Button recap;
    private Button autoEval;
    private ImageButton retour;
    private String nomEleve;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_eleve);

        //Affichage du nom de l'élève
        Intent sceance = getIntent();

        if (sceance != null){
            nomEleve = "";
            if (sceance.hasExtra("nomEleve")){ // vérifie qu'une valeur est associée à la clé
                nomEleve = sceance.getStringExtra("nomEleve"); // on récupère la valeur associée à la clé
            }
            TextView eleve = findViewById(R.id.nomEleveMenu);
            eleve.setText(nomEleve);
        }

        recap = findViewById(R.id.recap);
        autoEval = findViewById(R.id.autoEval);
        retour = findViewById(R.id.retourEleve2);

        recap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent recapIntent = new Intent(getApplicationContext(), MenuEleve.class);
                startActivity(recapIntent);
            }
        });

        autoEval.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent eval = new Intent(getApplicationContext(), Cours.class);
                startActivity(eval);
            }
        });

        retour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent retourEleve = new Intent(getApplicationContext(), AuthentificationEleve.class);
                startActivity(retourEleve);
            }
        });

    }
}
