package com.example.auto_valuation;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import androidx.appcompat.app.AppCompatActivity;

public class MenuProf extends AppCompatActivity {

    private Button listeEleve;
    private Button listeSport;
    private Button evaluation;
    private Button recapAutoEval;
    private Button exportCSV;
    private Button logOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_prof);

        listeEleve = findViewById(R.id.listeEleve);
        listeSport = findViewById(R.id.listeSport);
        evaluation = findViewById(R.id.evaluation);
        recapAutoEval  =findViewById(R.id.recapAutoEval);
        exportCSV = findViewById(R.id.exportToCSV);
        logOut = findViewById(R.id.logOut);

        listeEleve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent listEleve = new Intent(getApplicationContext(), ListeEleve.class);
                startActivity(listEleve);
            }
        });

        listeSport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent listSport = new Intent(getApplicationContext(), ListeSport.class);
                startActivity(listSport);
            }
        });

        evaluation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent eval = new Intent(getApplicationContext(), EvalProf.class);
                startActivity(eval);
            }
        });

        recapAutoEval.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent recap = new Intent(getApplicationContext(), RecapAutoEvaluation.class);
                startActivity(recap);
            }
        });

        exportCSV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent export = new Intent(getApplicationContext(), ListeSport.class);
                startActivity(export);
            }
        });

        logOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent deconnexion = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(deconnexion);
            }
        });
    }

}
