package com.example.auto_valuation.SQLite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

public class DatabaseManager extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "AutoEval.db";
    private static final int DATABASE_VERSION = 1;

    public DatabaseManager(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String tblProf = " create table prof ("
                       + " idProf integer primary key autoincrement,"
                       + " identifiant text not null,"
                       + " mdp text not null)";

        String tblEleve = " create table eleve ("
                        + " idEleve integer primary key autoincrement,"
                        + " nom text not null,"
                        + " prenom text not null,"
                        + " classe text not null,"
                        + " foreign key(idProf) references prof(idProf))";

        String tblSport = " create table sport ("
                        + " idSport integer primary key autoincrement,"
                        + " libelleSport text not null,"
                        + " nbSceance integer not null)";

        String tblComp = " create table competence ("
                       + " idCompetence integer primary key autoincrement,"
                       + " libelleCompetence text not null)";

        String linkPratique = " create table pratique("
                           + " idEleve integer not null,"
                           + " idSport integer not null,"
                           + " primary key(idEleve, idSport),"
                           + " foreign key(idEleve) references eleve(idEleve),"
                           + " foreign key(idSport) references sport(idSport))";

        String linkGere = " create table gere("
                       + " idProf integer not null,"
                       + " idSport integer not null,"
                       + " primary key(idProf, idSport),"
                       + " foreign key(idProf) references prof(idProf),"
                       + " foreign key(idSport) references sport(idSport))";

        String linkpossede = " create table possede("
                           + " idSport integer not null,"
                           + " idCompetence integer not null,"
                           + " primary key(idSport, idCompetence),"
                           + " foreign key(idSport) references sport(idSport),"
                           + " foreign key(idCompetence) references competence(idCompetence))";

        String linkAcquiere = " create table acquiere("
                            + " idEleve integer not null,"
                            + " idCompetence integer not null,"
                            + " noteCompetence integer not null,"
                            + " libelleNiveau text not null,"
                            + " primary key(idEleve, idCompetence),"
                            + " foreign key(idEleve) references eleve(idEleve),"
                            + " foreign key(idCompetence) references competence(idCompetence))";

        db.execSQL(tblProf);
        db.execSQL(tblEleve);
        db.execSQL(tblSport);
        db.execSQL(tblComp);
        db.execSQL(linkPratique);
        db.execSQL(linkGere);
        db.execSQL(linkpossede);
        db.execSQL(linkAcquiere);
        Log.i("DATABASE", "onCreate invoked");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //TODO
    }
}
