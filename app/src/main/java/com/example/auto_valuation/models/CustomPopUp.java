package com.example.auto_valuation.models;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import androidx.annotation.NonNull;

import com.example.auto_valuation.R;

public class CustomPopUp extends Dialog{

    private Spinner spinner;
    private Button validerpopUp;
    private Button retourPopUp;

    public CustomPopUp(Activity activity) {
        super(activity, R.style.Theme_AppCompat_DayNight_Dialog);
        setContentView(R.layout.popup_template);
        this.spinner = findViewById(R.id.spinnerPopUp);
        this.validerpopUp = findViewById(R.id.validerPopUp);
        this.retourPopUp = findViewById(R.id.retourPopUp);
    }

    public Button getValiderpopUp() {
        return this.validerpopUp;
    }

    public Button getRetourPopUp() {
        return this.retourPopUp;
    }

    public Spinner getSpinner() {
        return this.spinner;
    }

    public void build(){
        show();
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(), R.array.sport, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }
}
