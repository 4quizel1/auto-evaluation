package com.example.auto_valuation.models;

public class SportItem {

    private String sportName;

    public SportItem(String sportName){
        this.sportName = sportName;
    }

    public String getsportName(){return this.sportName;}

}
